%% 小波变换加水印，解水印大家按照加的思路逆过来就好

clc;clear;close all;
%% read data

im = double(imread('football.jpg'))/255;
mark = double(imread('watermark.png'))/255;
figure, imshow(im),title('original image');
figure, imshow(mark),title('watermark');
%RGB division
im=double(im); 
mark=double(mark); 
imr=im(:,:,1); 
markr=mark(:,:,1); 
img=im(:,:,2); 
markg=mark(:,:,2); 
imb=im(:,:,3); 
markb=mark(:,:,3); 
%parameter
r=0.01; 
g=0.01; 
b=0.01;
%wavelet tranform and add watermark
% for red
[Cwr,Swr]=wavedec2(markr,1,'haar'); 
[Cr,Sr]=wavedec2(imr,2,'haar'); 
% add watermark
Cr(1:size(Cwr,2)/16)=... 
Cr(1:size(Cwr,2)/16)+r*Cwr(1:size(Cwr,2)/16); 
k=0; 
while k<=size(Cr,2)/size(Cwr,2)-1 
Cr(1+size(Cr,2)/4+k*size(Cwr,2)/4:size(Cr,2)/4+... 
(k+1)*size(Cwr,2)/4)=Cr(1+size(Cr,2)/4+... 
k*size(Cwr,2)/4:size(Cr,2)/4+(k+1)*size(Cwr,2)/4)+... 
r*Cwr(1+size(Cwr,2)/4:size(Cwr,2)/2); 
Cr(1+size(Cr,2)/2+k*size(Cwr,2)/4:size(Cr,2)/2+... 
(k+1)*size(Cwr,2)/4)=Cr(1+size(Cr,2)/2+... 
k*size(Cwr,2)/4:size(Cr,2)/2+(k+1)*size(Cwr,2)/4)+... 
r*Cwr(1+size(Cwr,2)/2:3*size(Cwr,2)/4); 
Cr(1+3*size(Cwr,2)/4+k*size(Cwr,2)/4:3*size(Cwr,2)/4+... 
(k+1)*size(Cwr,2)/4)=Cr(1+3*size(Cr,2)/4+... 
k*size(Cwr,2)/4:3*size(Cr,2)/4+(k+1)*size(Cwr,2)/4)+... 
r*Cwr(1+3*size(Cwr,2)/4:size(Cwr,2)); 
k=k+1; 
end 
Cr(1:size(Cwr,2)/4)=Cr(1:size(Cwr,2)/4)+r*Cwr(1:size(Cwr,2)/4); 

% for green
[Cwg,Swg]=wavedec2(markg,1,'haar'); 
[Cg,Sg]=wavedec2(img,2,'haar'); 
Cg(1:size(Cwg,2)/16)=... 
Cg(1:size(Cwg,2)/16)+g*Cwg(1:size(Cwg,2)/16); 
k=0; 
while k<=size(Cg,2)/size(Cwg,2)-1 
Cg(1+size(Cg,2)/4+k*size(Cwg,2)/4:size(Cg,2)/4+... 
(k+1)*size(Cwg,2)/4)=Cg(1+size(Cg,2)/4+... 
k*size(Cwg,2)/4:size(Cg,2)/4+(k+1)*size(Cwg,2)/4)+... 
g*Cwg(1+size(Cwg,2)/4:size(Cwg,2)/2); 
Cg(1+size(Cg,2)/2+k*size(Cwg,2)/4:size(Cg,2)/2+... 
(k+1)*size(Cwg,2)/4)=Cg(1+size(Cg,2)/2+... 
k*size(Cwg,2)/4:size(Cg,2)/2+(k+1)*size(Cwg,2)/4)+... 
g*Cwg(1+size(Cwg,2)/2:3*size(Cwg,2)/4); 
Cg(1+3*size(Cg,2)/4+k*size(Cwg,2)/4:3*size(Cg,2)/4+... 
(k+1)*size(Cwg,2)/4)=Cg(1+3*size(Cg,2)/4+... 
k*size(Cwg,2)/4:3*size(Cg,2)/4+(k+1)*size(Cwg,2)/4)+... 
g*Cwg(1+3*size(Cwg,2)/4:size(Cwg,2)); 
k=k+1; 
end 
Cg(1:size(Cwg,2)/4)=Cg(1:size(Cwg,2)/4)+g*Cwg(1:size(Cwg,2)/4); 

% for blue
[Cwb,Swb]=wavedec2(markb,1,'haar'); 
[Cb,Sb]=wavedec2(imb,2,'haar'); 
Cb(1:size(Cwb,2)/16)=Cb(1:size(Cwb,2)/16)+b*Cwb(1:size(Cwb,2)/16); 
k=0; 
while k<=size(Cb,2)/size(Cwb,2)-1 
Cb(1+size(Cb,2)/4+k*size(Cwb,2)/4:size(Cb,2)/4+... 
(k+1)*size(Cwb,2)/4)=Cb(1+size(Cb,2)/4+... 
k*size(Cwb,2)/4:size(Cb,2)/4+(k+1)*size(Cwb,2)/4)+... 
g*Cwb(1+size(Cwb,2)/4:size(Cwb,2)/2); 
Cb(1+size(Cb,2)/2+k*size(Cwb,2)/4:size(Cb,2)/2+... 
(k+1)*size(Cwb,2)/4)=Cb(1+size(Cb,2)/2+... 
k*size(Cwb,2)/4:size(Cb,2)/2+(k+1)*size(Cwb,2)/4)+... 
b*Cwb(1+size(Cwb,2)/2:3*size(Cwb,2)/4); 
Cb(1+3*size(Cb,2)/4+k*size(Cwb,2)/4:3*size(Cb,2)/4+... 
(k+1)*size(Cwb,2)/4)=Cb(1+3*size(Cb,2)/4+... 
k*size(Cwb,2)/4:3*size(Cb,2)/4+(k+1)*size(Cwb,2)/4)+... 
b*Cwb(1+3*size(Cwb,2)/4:size(Cwb,2)); 
k=k+1; 
end 
Cb(1:size(Cwb,2)/4)=Cb(1:size(Cwb,2)/4)+b*Cwb(1:size(Cwb,2)/4); 
% image reconstruction
% imr=WAVEREC2(Cr,Sr,'haar'); 
% img=WAVEREC2(Cg,Sg,'haar'); 
% imb=WAVEREC2(Cb,Sb,'haar'); 
imr1=waverec2(Cr,Sr,'haar');
img1=waverec2(Cg,Sg,'haar');
imb1=waverec2(Cb,Sb,'haar');
imsize=size(imr); 
FAO=zeros(imsize(1),imsize(2),3); 
for i=1:imsize(1) 
for j=1:imsize(2) 
FAO(i,j,1)=imr1(i,j); 
FAO(i,j,2)=img1(i,j); 
FAO(i,j,3)=imb1(i,j); 
end 
end 
figure, imshow(FAO); title('watermarked image');


% Cb(1:size(Cwb,2)/4)=Cb(1:size(Cwb,2)/4)-b*Cwb(1:size(Cwb,2)/4); 
Cwb(1:size(Cwb,2)/4)=-(Cb(1:size(Cwb,2)/4)-Cb(1:size(Cwb,2)/4))/b;
% % Cg(1:size(Cwg,2)/4)=Cg(1:size(Cwg,2)/4)-g*Cwg(1:size(Cwg,2)/4); 
Cwg(1:size(Cwg,2)/4)=-(Cg(1:size(Cwg,2)/4)-Cg(1:size(Cwg,2)/4))/g;

% % Cr(1:size(Cwr,2)/4)=Cr(1:size(Cwr,2)/4)-r*Cwr(1:size(Cwr,2)/4); 
Cwr(1:size(Cwr,2)/4)=-(Cr(1:size(Cwr,2)/4)-Cr(1:size(Cwr,2)/4))/r;

Cwr(1:size(Cwr,2)/16)=-(Cr(1:size(Cwr,2)/16)-Cr(1:size(Cwr,2)/16))/r;
Cwg(1:size(Cwg,2)/16)=-(Cg(1:size(Cwg,2)/16)-Cg(1:size(Cwg,2)/16))/g;
Cwb(1:size(Cwb,2)/16)=-(Cb(1:size(Cwb,2)/16)-Cb(1:size(Cwb,2)/16))/b;

imr=waverec2(Cwr,Swr,'haar');
img=waverec2(Cwg,Swg,'haar');
imb=waverec2(Cwb,Swb,'haar');
imsize=size(imr); 
WAT=zeros(imsize(1),imsize(2),3); 
for i=1:imsize(1) 
for j=1:imsize(2)
WAT(i,j,1)=imr(i,j); 
WAT(i,j,2)=img(i,j); 
WAT(i,j,3)=imb(i,j); 
end 
end 
figure, imshow(WAT); title('watermark image');
%% 攻击

attack = 1-double(imread('attack.jpg'))/255;
figure,imshow(attack);title('涂抹攻击');
FAO_ = FAO;
attsize = size(attack);
for i=1:attsize(1)
    for j=1:attsize(2)
        if attack(i,j,1)+attack(i,j,2)+attack(i,j,3)>0.3
            FAO_(i,j,:) = attack(i,j,:);
        end
    end
end
figure,imshow(FAO_);
%RGB division
im=double(FAO_); 
imr=im(:,:,1); 
img=im(:,:,2); 
imb=im(:,:,3); 
[Cr1,~]=wavedec2(imr,2,'haar'); 
[Cg1,~]=wavedec2(img,2,'haar'); 
[Cb1,~]=wavedec2(imb,2,'haar'); 
% mark=double(mark); 
% markr=mark(:,:,1); 
% markg=mark(:,:,2); 
% markb=mark(:,:,3); 
% [Cwr,Swr]=wavedec2(markr,1,'haar'); 
% [Cwg,Swg]=wavedec2(markg,1,'haar'); 
% [Cwg,Swg]=wavedec2(markg,1,'haar'); 
Cwr(1:size(Cwr,2)/4)=(Cr1(1:size(Cwr,2)/4)-Cr(1:size(Cwr,2)/4))/r;
Cwg(1:size(Cwg,2)/4)=(Cg1(1:size(Cwg,2)/4)-Cg(1:size(Cwg,2)/4))/g;
Cwb(1:size(Cwb,2)/4)=(Cb1(1:size(Cwb,2)/4)-Cb(1:size(Cwb,2)/4))/b;

Cwr(1:size(Cwr,2)/16)=-(Cr(1:size(Cwr,2)/16)-Cr1(1:size(Cwr,2)/16))/r;
Cwg(1:size(Cwg,2)/16)=-(Cg(1:size(Cwg,2)/16)-Cg1(1:size(Cwg,2)/16))/g;
Cwb(1:size(Cwb,2)/16)=-(Cb(1:size(Cwb,2)/16)-Cb1(1:size(Cwb,2)/16))/b;
imr=waverec2(Cwr,Swr,'haar');
img=waverec2(Cwg,Swg,'haar');
imb=waverec2(Cwb,Swb,'haar');
imsize=size(imr); 
WAT=zeros(imsize(1),imsize(2),3); 
for i=1:imsize(1) 
for j=1:imsize(2) 
WAT(i,j,1)=imr(i,j); 
WAT(i,j,2)=img(i,j); 
WAT(i,j,3)=imb(i,j); 
end 
end 
figure, imshow(WAT); title('watermark image');
%% 裁剪攻击

s2 = 0.8;
FAO_ = FAO;
FAO_(:,s2*imsize(2)+1:imsize(2),:) = FAO_(:,1:int32((1-s2)*imsize(2)),:);
figure,imshow(FAO_);title('剪贴后的图像');
im=double(FAO_); 
imr=im(:,:,1); 
img=im(:,:,2); 
imb=im(:,:,3); 
[Cr1,~]=wavedec2(imr,2,'haar'); 
[Cg1,~]=wavedec2(img,2,'haar'); 
[Cb1,~]=wavedec2(imb,2,'haar'); 
Cwr(1:size(Cwr,2)/4)=(Cr1(1:size(Cwr,2)/4)-Cr(1:size(Cwr,2)/4))/r;
Cwg(1:size(Cwg,2)/4)=(Cg1(1:size(Cwg,2)/4)-Cg(1:size(Cwg,2)/4))/g;
Cwb(1:size(Cwb,2)/4)=(Cb1(1:size(Cwb,2)/4)-Cb(1:size(Cwb,2)/4))/b;

Cwr(1:size(Cwr,2)/16)=-(Cr(1:size(Cwr,2)/16)-Cr1(1:size(Cwr,2)/16))/r;
Cwg(1:size(Cwg,2)/16)=-(Cg(1:size(Cwg,2)/16)-Cg1(1:size(Cwg,2)/16))/g;
Cwb(1:size(Cwb,2)/16)=-(Cb(1:size(Cwb,2)/16)-Cb1(1:size(Cwb,2)/16))/b;
imr=waverec2(Cwr,Swr,'haar');
img=waverec2(Cwg,Swg,'haar');
imb=waverec2(Cwb,Swb,'haar');
imsize=size(imr); 
WAT=zeros(imsize(1),imsize(2),3); 
for i=1:imsize(1) 
for j=1:imsize(2) 
WAT(i,j,1)=imr(i,j); 
WAT(i,j,2)=img(i,j); 
WAT(i,j,3)=imb(i,j); 
end 
end 
figure, imshow(WAT); title('watermark image');
%% 旋转攻击

FAO_ = imrotate(FAO,90);
figure,imshow(FAO_);title('旋转后的图像');
im=double(FAO_); 
imr=im(:,:,1); 
img=im(:,:,2); 
imb=im(:,:,3); 
[Cr1,~]=wavedec2(imr,2,'haar'); 
[Cg1,~]=wavedec2(img,2,'haar'); 
[Cb1,~]=wavedec2(imb,2,'haar'); 
Cwr(1:size(Cwr,2)/4)=(Cr1(1:size(Cwr,2)/4)-Cr(1:size(Cwr,2)/4))/r;
Cwg(1:size(Cwg,2)/4)=(Cg1(1:size(Cwg,2)/4)-Cg(1:size(Cwg,2)/4))/g;
Cwb(1:size(Cwb,2)/4)=(Cb1(1:size(Cwb,2)/4)-Cb(1:size(Cwb,2)/4))/b;

Cwr(1:size(Cwr,2)/16)=-(Cr(1:size(Cwr,2)/16)-Cr1(1:size(Cwr,2)/16))/r;
Cwg(1:size(Cwg,2)/16)=-(Cg(1:size(Cwg,2)/16)-Cg1(1:size(Cwg,2)/16))/g;
Cwb(1:size(Cwb,2)/16)=-(Cb(1:size(Cwb,2)/16)-Cb1(1:size(Cwb,2)/16))/b;
imr=waverec2(Cwr,Swr,'haar');
img=waverec2(Cwg,Swg,'haar');
imb=waverec2(Cwb,Swb,'haar');
imsize=size(imr); 
WAT=zeros(imsize(1),imsize(2),3); 
for i=1:imsize(1) 
for j=1:imsize(2) 
WAT(i,j,1)=imr(i,j); 
WAT(i,j,2)=img(i,j); 
WAT(i,j,3)=imb(i,j); 
end 
end 
figure, imshow(WAT); title('watermark image');

%% 滤波攻击


FAO_ = imnoise(FAO,'salt & pepper',0.01);
figure,imshow(FAO_);
im=double(FAO_); 
imr=im(:,:,1); 
img=im(:,:,2); 
imb=im(:,:,3); 
[Cr1,Sr1]=wavedec2(imr,2,'haar'); 
[Cg1,Sg1]=wavedec2(img,2,'haar'); 
[Cb1,Sb1]=wavedec2(imb,2,'haar'); 
Cwr(1:size(Cwr,2)/4)=(Cr1(1:size(Cwr,2)/4)-Cr(1:size(Cwr,2)/4))/r;
Cwg(1:size(Cwg,2)/4)=(Cg1(1:size(Cwg,2)/4)-Cg(1:size(Cwg,2)/4))/g;
Cwb(1:size(Cwb,2)/4)=(Cb1(1:size(Cwb,2)/4)-Cb(1:size(Cwb,2)/4))/b;

Cwr(1:size(Cwr,2)/16)=-(Cr(1:size(Cwr,2)/16)-Cr1(1:size(Cwr,2)/16))/r;
Cwg(1:size(Cwg,2)/16)=-(Cg(1:size(Cwg,2)/16)-Cg1(1:size(Cwg,2)/16))/g;
Cwb(1:size(Cwb,2)/16)=-(Cb(1:size(Cwb,2)/16)-Cb1(1:size(Cwb,2)/16))/b;
imr=waverec2(Cwr,Swr,'haar');
img=waverec2(Cwg,Swg,'haar');
imb=waverec2(Cwb,Swb,'haar');
imsize=size(imr); 
WAT=zeros(imsize(1),imsize(2),3); 
for i=1:imsize(1) 
for j=1:imsize(2) 
WAT(i,j,1)=imr(i,j); 
WAT(i,j,2)=img(i,j); 
WAT(i,j,3)=imb(i,j); 
end 
end 
figure, imshow(WAT); title('watermark image');