 I=imread('pout.tif');  
noise_jy=imnoise(I,'salt & pepper',0.02);     %添加椒盐噪声
noise_gs=imnoise(I,'gaussian');               %添加高斯噪声 

way=inputdlg('输入滤波器种类average,med,gaussian,laplacian&prewitt,log&sobel','图像空域滤波');
%前三为平滑滤波器，后四为锐化滤波器
way=char(way);
switch way
    case 'average'
        K3=filter2(fspecial('average',3),noise_jy);
        K4=filter2(fspecial('average',7),noise_jy);
        K5=filter2(fspecial('average',3),noise_gs);
        K6=filter2(fspecial('average',7),noise_gs);
        K1=filter2(fspecial('average',3),I);
        K2=filter2(fspecial('average',7),I);
        subplot(334),imshow(K1,[]),title('模板尺寸为3的均值滤波器');
        subplot(337),imshow(K2,[]),title('模板尺寸为7的均值滤波器');
        subplot(335),imshow(K3,[]),title('模板尺寸为3的均值滤波器');
        subplot(338),imshow(K4,[]),title('模板尺寸为7的均值滤波器');
        subplot(336),imshow(K5,[]),title('模板尺寸为3的均值滤波器');
        subplot(339),imshow(K6,[]),title('模板尺寸为7的均值滤波器');
    case 'gaussian'
        K1=imgaussfilt(I);
        K2=imgaussfilt(I,2);
        K3=imgaussfilt(noise_jy);
        K4=imgaussfilt(noise_jy,2);
        K5=imgaussfilt(noise_gs);
        K6=imgaussfilt(noise_gs,2);
        subplot(334),imshow(K1,[]),title('标准差为0.5的高斯滤波器');
        subplot(337),imshow(K2,[]),title('标准差为2的高斯滤波器');
        subplot(335),imshow(K3,[]),title('标准差为0.5的高斯滤波器');
        subplot(338),imshow(K4,[]),title('标准差为2的高斯滤波器');
        subplot(336),imshow(K5,[]),title('标准差为0.5的高斯滤波器');
        subplot(339),imshow(K6,[]),title('标准差为2的高斯滤波器');
    case 'med'
        K1=medfilt2(I,[5 5]);
        K2=medfilt2(I,[10 10]);
        K3=medfilt2(noise_jy,[5 5]);
        K4=medfilt2(noise_jy,[10 10]);
        K5=medfilt2(noise_gs,[5 5]);
        K6=medfilt2(noise_gs,[10 10]);
        subplot(334),imshow(K1,[]),title('尺寸为5的中值滤波');
        subplot(337),imshow(K2,[]),title('尺寸为10的中值滤波');
        subplot(335),imshow(K3,[]),title('尺寸为5的中值滤波');
        subplot(338),imshow(K4,[]),title('尺寸为10的中值滤波');
        subplot(336),imshow(K5,[]),title('尺寸为5的中值滤波');
        subplot(339),imshow(K6,[]),title('尺寸为10的中值滤波');
    case 'laplacian&prewitt'
        K3=filter2(fspecial('laplacian'),noise_jy);
        K4=filter2(fspecial('prewitt'),noise_jy);
        K5=filter2(fspecial('laplacian'),noise_gs);
        K6=filter2(fspecial('prewitt'),noise_gs);
        K1=filter2(fspecial('laplacian'),I);
        K2=filter2(fspecial('prewitt'),I);
        subplot(334),imshow(K1,[]),title('lapacian算子的锐化滤波');
        subplot(337),imshow(K2,[]),title('peteitt算子的锐化滤波');
        subplot(335),imshow(K3,[]),title('lapacian算子的锐化滤波');
        subplot(338),imshow(K4,[]),title('peteitt算子的锐化滤波');
        subplot(336),imshow(K5,[]),title('lapacian算子的锐化滤波');
        subplot(339),imshow(K6,[]),title('peteitt算子的锐化滤波');
    case 'log&sobel'
        K3=filter2(fspecial('log'),noise_jy);
        K4=filter2(fspecial('sobel'),noise_jy);
        K5=filter2(fspecial('log'),noise_gs);
        K6=filter2(fspecial('sobel'),noise_gs);
        K1=filter2(fspecial('log'),I);
        K2=filter2(fspecial('sobel'),I);
        subplot(334),imshow(K1,[]),title('log算子的锐化滤波');
        subplot(337),imshow(K2,[]),title('sobel算子的锐化滤波');
        subplot(335),imshow(K3,[]),title('log算子的锐化滤波');
        subplot(338),imshow(K4,[]),title('sobel算子的锐化滤波');
        subplot(336),imshow(K5,[]),title('log算子的锐化滤波');
        subplot(339),imshow(K6,[]),title('sobel算子的锐化滤波');
        
    otherwise
        errordlg('听不懂你说啥,嚇嚇~');
end

subplot(331),imshow(I),title('原始图像');  
subplot(332),imshow(noise_jy),title('加入椒盐噪声的图像'); 
subplot(333),imshow(noise_gs),title('加入高斯噪声的图像的图像'); 