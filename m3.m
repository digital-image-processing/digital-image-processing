img = im2double(imread('pout.tif'));
%% 加噪声
[a,b] = size(img);
img_noisy1 = imnoise(img,'gaussian',0,0.01);%高斯噪声

%img_noisy1 = imnoise(img,'salt & pepper',0.05);%椒盐噪声
img1 = img_noisy1;

%% 
img_noisy1=fft2(img_noisy1);%利用fft2()函数将图像从时域空间转换到频域空间
img_noisy1=fftshift(img_noisy1);%将零频平移到中心位置
[m,n]=size(img_noisy1);
m_min=round(m/2);
n_min=round(n/2);
t_rf=img_noisy1;
d_0=100;%设置阈值
for i=1:m
for j=1:n
d_1=sqrt((i-m_min)^2+(j-n_min)^2);
if(d_1>d_0)
x=0;
else 
x=1.5; %增强
end
t_rf(i,j)=x*img_noisy1(i,j);
end
end
t_rf=ifftshift(t_rf);
t_rf=ifft2(t_rf);
% figure,imshow(img1);
% figure,imshow(t_rf);
subplot(1,2,1);
            imshow(img1); %显示原始图像
            title('原始图像');
            subplot(1,2,2);
            imshow(t_rf); %显示伽马变换后图像
            title('幂律变换后图像');
